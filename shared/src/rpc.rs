pub use crate::serde::{Code, Status};
use serde::{Deserialize, Serialize};

pub fn binary_encode_prost_message<T: prost::Message>(message: T) -> Vec<u8> {
    let mut buf: Vec<u8> = Vec::new();
    message.encode(&mut buf).unwrap();
    buf
}

pub fn binary_decode_prost_message<T: prost::Message + Default>(
    bytes: Vec<u8>,
) -> Result<T, prost::DecodeError> {
    T::decode(<prost::bytes::Bytes>::from(bytes))
}

type MessageOrStatus<T> = Result<T, Status>;

pub fn binary_encode_prost_message_or_status<T: prost::Message + Serialize>(
    message_or: MessageOrStatus<T>,
) -> Vec<u8> {
    bincode::serialize(&message_or).unwrap()
}

pub fn binary_decode_prost_message_or_status<'a, T: prost::Message + Deserialize<'a> + Default>(
    bytes: &'a [u8],
) -> MessageOrStatus<T> {
    bincode::deserialize(bytes).unwrap()
}
