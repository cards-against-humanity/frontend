use super::super::helper::AsyncOption;
use super::super::pages::{push_route, AppRoute};
use crate::proto::crusty_cards_api::*;
use yew::prelude::*;
use yew_material::{MatCircularProgress, MatIcon, MatIconButton, MatTopAppBar};
use yewtil::NeqAssign;

pub struct NavBar {
    props: NavBarProps,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct NavBarProps {
    pub current_user: AsyncOption<User>,
    pub children: Children,
}

pub enum NavBarMsg {
    ProfileNavButtonClicked,
    CurrentGameNavButtonClicked,
    GameListNavButtonClicked,
    SettingsNavButtonClicked,
    LogoutNavButtonClicked,
}

impl Component for NavBar {
    type Message = NavBarMsg;
    type Properties = NavBarProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            NavBarMsg::ProfileNavButtonClicked => {
                match &self.props.current_user {
                    AsyncOption::Some(user) => push_route(AppRoute::UserPage(
                        user.name.split("/").last().unwrap().to_string(),
                    )),
                    _ => {} // TODO - We should create a snackbar message that displays the error that occured and why the user was not redirected where they expected.
                }
            }
            NavBarMsg::CurrentGameNavButtonClicked => push_route(AppRoute::CurrentGamePage),
            NavBarMsg::GameListNavButtonClicked => push_route(AppRoute::GameListPage),
            NavBarMsg::SettingsNavButtonClicked => push_route(AppRoute::SettingsPage),
            NavBarMsg::LogoutNavButtonClicked => push_route(AppRoute::LogoutPage), // TODO - We want to refresh the page when logging out, so the server can clear the auth cookie. Right now this won't actually log the user out.
        };
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        html! {
            <MatTopAppBar center_title=false dense=false prominent=false>
                <div slot="title">{ "Crusty Cards" }</div>
                {
                    match &self.props.current_user {
                        AsyncOption::Loading => {
                            html! {
                                <span slot="actionItems">
                                    <MatCircularProgress indeterminate=true/>
                                </span>
                            }
                        },
                        AsyncOption::Error => {
                            html! {
                                <span slot="actionItems">
                                    <MatIcon>{"error"}</MatIcon>
                                </span>
                            }
                        },
                        AsyncOption::Some(user) => {
                            // TODO - Add tooltip text when hovering over one of the action items.
                            html! {
                                <>
                                    <span slot="actionItems" onclick=self.link.callback(|_| NavBarMsg::ProfileNavButtonClicked)>
                                        <MatIconButton icon="person"/>
                                    </span>
                                    <span slot="actionItems" onclick=self.link.callback(|_| NavBarMsg::CurrentGameNavButtonClicked)>
                                        <MatIconButton icon="videogame_asset"/>
                                    </span>
                                    <span slot="actionItems" onclick=self.link.callback(|_| NavBarMsg::GameListNavButtonClicked)>
                                        <MatIconButton icon="view_list"/>
                                    </span>
                                    <span slot="actionItems" onclick=self.link.callback(|_| NavBarMsg::SettingsNavButtonClicked)>
                                        <MatIconButton icon="settings"/>
                                    </span>
                                    <a slot="actionItems" href="/logout" style="color: inherit">
                                        <MatIconButton icon="exit_to_app"/>
                                    </a>
                                </>
                            }
                        },
                        AsyncOption::None => {
                            html! {}
                        }
                    }
                }
                { self.props.children.clone() }
            </MatTopAppBar>
        }
    }
}
