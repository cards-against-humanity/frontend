use std::include_bytes;
use yew::prelude::*;

const GOOGLE_OAUTH_BUTTON_IMAGE_FOCUS: &'static [u8] =
    include_bytes!("../content/btn_google_signin_dark_focus_web.png");
const GOOGLE_OAUTH_BUTTON_IMAGE_NORMAL: &'static [u8] =
    include_bytes!("../content/btn_google_signin_dark_normal_web.png");
const GOOGLE_OAUTH_BUTTON_IMAGE_PRESSED: &'static [u8] =
    include_bytes!("../content/btn_google_signin_dark_pressed_web.png");

// TODO - Make this button match the one on Google's official guide: https://developers.google.com/identity/sign-in/web/build-button

pub struct GoogleOAuthButton {
    link: ComponentLink<Self>,
    is_hovering: bool,
    mouse_down: bool,
}

pub enum GoogleOAuthButtonMsg {
    MouseOver,
    MouseOut,
    MouseDown,
    MouseUp,
}

impl Component for GoogleOAuthButton {
    type Message = GoogleOAuthButtonMsg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            is_hovering: false,
            mouse_down: false,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Self::Message::MouseOver => {
                self.is_hovering = true;
            }
            Self::Message::MouseOut => {
                self.is_hovering = false;
                self.mouse_down = false;
            }
            Self::Message::MouseDown => {
                self.is_hovering = true;
                self.mouse_down = true;
            }
            Self::Message::MouseUp => {
                self.mouse_down = false;
            }
        };
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        let base64_image_bytes = if self.mouse_down {
            base64::encode(GOOGLE_OAUTH_BUTTON_IMAGE_PRESSED)
        } else if self.is_hovering {
            base64::encode(GOOGLE_OAUTH_BUTTON_IMAGE_FOCUS)
        } else {
            base64::encode(GOOGLE_OAUTH_BUTTON_IMAGE_NORMAL)
        };

        html! {
            <a href="/auth/google" style="-webkit-user-drag: none">
                <img
                    onmouseover=self.link.callback(|_| GoogleOAuthButtonMsg::MouseOver)
                    onmouseout=self.link.callback(|_| GoogleOAuthButtonMsg::MouseOut)
                    onmousedown=self.link.callback(|_| GoogleOAuthButtonMsg::MouseDown)
                    onmouseup=self.link.callback(|_| GoogleOAuthButtonMsg::MouseUp)
                    src=format!("data:image/png;base64,{}", base64_image_bytes)
                    style="-webkit-user-drag: none"
                />
            </a>
        }
    }
}
