use super::{AppState, AppStateHandle};
use yew::prelude::*;
use yew_state::{SharedHandle, SharedState, SharedStateComponent};
use yewtil::NeqAssign;

pub struct DefaultCardpackListPage {
    props: DefaultCardpackListPageProps,
}

#[derive(Properties, Clone, PartialEq)]
pub struct DefaultCardpackListPageProps {
    #[prop_or_default]
    pub app_state: AppStateHandle,
}

impl SharedState for DefaultCardpackListPageProps {
    type Handle = SharedHandle<AppState>;
    fn handle(&mut self) -> &mut AppStateHandle {
        &mut self.app_state
    }
}

impl Component for DefaultCardpackListPage {
    type Message = ();
    type Properties = DefaultCardpackListPageProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        html! {
            <div>
                { "Welcome to the default cardpack list page!" }
            </div>
        }
    }
}

pub type SharedStateDefaultCardpackListPage = SharedStateComponent<DefaultCardpackListPage>;
