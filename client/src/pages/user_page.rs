use super::super::components::{
    error_page_overlay::ErrorPageOverlay, loading_page_overlay::LoadingPageOverlay,
};
use super::super::helper::{convert_to_async_option, AsyncOption};
use super::{AppState, AppStateHandle};
use crate::proto::crusty_cards_api::User;
use crate::user_service::get_user;
use cards_frontend_shared::rpc::Status;
use yew::prelude::*;
use yew_services::fetch::FetchTask;
use yew_state::{SharedHandle, SharedState, SharedStateComponent, StateHandle};
use yewtil::NeqAssign;

pub struct UserPage {
    props: UserPageProps,
    // This task handle is only used to prevent the request from being cancelled.
    #[allow(dead_code)]
    get_user_task_or: Option<FetchTask>,
    user: AsyncOption<User>,
}

#[derive(Properties, Clone, PartialEq)]
pub struct UserPageProps {
    #[prop_or_default]
    pub app_state: AppStateHandle,
    #[prop_or_default]
    pub user_name: String,
}

impl SharedState for UserPageProps {
    type Handle = SharedHandle<AppState>;
    fn handle(&mut self) -> &mut AppStateHandle {
        &mut self.app_state
    }
}

pub enum UserPageMsg {
    UpdateUser(AsyncOption<User>),
}

impl UserPage {
    fn create_with_fetch_request(props: UserPageProps, link: ComponentLink<Self>) -> Self {
        let get_user_task = get_user(
            props.user_name.clone(),
            link.callback_once(|res: Result<User, Status>| {
                UserPageMsg::UpdateUser(convert_to_async_option(res))
            }),
        );

        Self {
            props,
            get_user_task_or: Some(get_user_task),
            user: AsyncOption::Loading,
        }
    }
}

impl Component for UserPage {
    type Message = UserPageMsg;
    type Properties = UserPageProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        match &props.app_state.state().current_user {
            AsyncOption::Some(current_user) => {
                // If the user is viewing their own profile page, we don't need to
                // refetch their user data since we already have it in local state.
                if current_user.name == props.user_name {
                    let user = current_user.clone();
                    Self {
                        props,
                        get_user_task_or: None,
                        user: AsyncOption::Some(user),
                    }
                } else {
                    Self::create_with_fetch_request(props, link)
                }
            }
            _ => Self::create_with_fetch_request(props, link),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            UserPageMsg::UpdateUser(user_or) => self.user = user_or,
        };
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        let user = match &self.user {
            AsyncOption::Loading => return html! { <LoadingPageOverlay/> },
            AsyncOption::Error => return html! { <ErrorPageOverlay/> },
            AsyncOption::None => return html! { "User does not exist!" }, // TODO - Make the 404 case more than just some text.
            AsyncOption::Some(user) => user,
        };

        html! {
            <div>
                { format!("Welcome to the user page for {}!", user.display_name) }
            </div>
        }
    }
}

pub type SharedStateUserPage = SharedStateComponent<UserPage>;
