use super::AppStateHandle;
use yew::prelude::*;
use yew_state::SharedStateComponent;
use yewtil::NeqAssign;

pub struct SettingsPage {
    app_state: AppStateHandle,
}

impl Component for SettingsPage {
    type Message = ();
    type Properties = AppStateHandle;

    fn create(app_state: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { app_state }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, app_state: Self::Properties) -> ShouldRender {
        self.app_state.neq_assign(app_state)
    }

    fn view(&self) -> Html {
        html! {
            <div>
                { "Welcome to the settings page!" }
            </div>
        }
    }
}

pub type SharedStateSettingsPage = SharedStateComponent<SettingsPage>;
