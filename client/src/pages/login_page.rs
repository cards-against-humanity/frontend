use super::super::components::google_oauth_button::GoogleOAuthButton;
use super::super::components::{
    error_page_overlay::ErrorPageOverlay, loading_page_overlay::LoadingPageOverlay,
};
use super::replace_route;
use super::{AppRoute, AppStateHandle, AsyncOption};
use yew::prelude::*;
use yew_state::{SharedStateComponent, StateHandle};
use yewtil::NeqAssign;

pub struct LoginPage {
    app_state: AppStateHandle,
}

impl Component for LoginPage {
    type Message = ();
    type Properties = AppStateHandle;

    fn create(app_state: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { app_state }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, app_state: Self::Properties) -> ShouldRender {
        self.app_state.neq_assign(app_state)
    }

    fn view(&self) -> Html {
        match &self.app_state.state().current_user {
            AsyncOption::Loading => {
                html! {
                    <div>
                        <LoadingPageOverlay/>
                    </div>
                }
            }
            AsyncOption::Error => {
                html! {
                    <div>
                        <ErrorPageOverlay/>
                    </div>
                }
            }
            AsyncOption::None => {
                html! {
                    <div style="padding: 2em; text-align: center">
                        <GoogleOAuthButton/>
                    </div>
                }
            }
            AsyncOption::Some(_user) => {
                replace_route(AppRoute::GameListPage);
                html! {
                    <div>
                        <LoadingPageOverlay/>
                    </div>
                }
            }
        }
    }
}

pub type SharedStateLoginPage = SharedStateComponent<LoginPage>;
