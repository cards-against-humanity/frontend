use super::{AppState, AppStateHandle};
use yew::prelude::*;
use yew_state::{SharedHandle, SharedState, SharedStateComponent};
use yewtil::NeqAssign;

pub struct CustomCardpackPage {
    props: CustomCardpackPageProps,
}

#[derive(Properties, Clone, PartialEq)]
pub struct CustomCardpackPageProps {
    #[prop_or_default]
    pub app_state: AppStateHandle,
    #[prop_or_default]
    pub user_name: String,
    #[prop_or_default]
    pub custom_cardpack_name: String,
}

impl SharedState for CustomCardpackPageProps {
    type Handle = SharedHandle<AppState>;
    fn handle(&mut self) -> &mut AppStateHandle {
        &mut self.app_state
    }
}

impl Component for CustomCardpackPage {
    type Message = ();
    type Properties = CustomCardpackPageProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        html! {
            <div>
                { format!("Welcome to the custom cardpack page! Owner: {}  Cardpack: {}", self.props.user_name, self.props.custom_cardpack_name) }
            </div>
        }
    }
}

pub type SharedStateCustomCardpackPage = SharedStateComponent<CustomCardpackPage>;
