use super::AppStateHandle;
use yew::prelude::*;
use yew_material::{
    dialog::{ActionType, MatDialogAction},
    MatButton, MatDialog, MatFab, WeakComponentLink,
};
use yew_state::SharedStateComponent;
use yewtil::NeqAssign;

pub struct GameListPage {
    app_state: AppStateHandle,
    link: ComponentLink<Self>,
    create_game_dialog_link: WeakComponentLink<MatDialog>,
}

pub enum GameListPageMsg {
    ShowCreateGameDialog,
}

impl Component for GameListPage {
    type Message = GameListPageMsg;
    type Properties = AppStateHandle;

    fn create(app_state: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            app_state,
            link,
            create_game_dialog_link: Default::default(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            GameListPageMsg::ShowCreateGameDialog => {
                self.create_game_dialog_link.show();
                false
            }
        }
    }

    fn change(&mut self, app_state: Self::Properties) -> ShouldRender {
        self.app_state.neq_assign(app_state)
    }

    fn view(&self) -> Html {
        html! {
            <div>
                { "Welcome to the game list page!" }
                <span
                    onclick=self.link.callback(|_| GameListPageMsg::ShowCreateGameDialog)
                    style="right: 2em; bottom: 2em; position: fixed"
                >
                    <MatFab icon="add"/>
                </span>
                <MatDialog heading="Create Game" dialog_link=self.create_game_dialog_link.clone()>
                    { "Create a game here!" }
                    <MatDialogAction action_type=ActionType::Primary action="close">
                        <MatButton label="Close this!" />
                    </MatDialogAction>
                </MatDialog>
            </div>
        }
    }
}

pub type SharedStateGameListPage = SharedStateComponent<GameListPage>;
