use super::{AppState, AppStateHandle};
use yew::prelude::*;
use yew_state::{SharedHandle, SharedState, SharedStateComponent};
use yewtil::NeqAssign;

pub struct DefaultCardpackPage {
    props: DefaultCardpackPageProps,
}

#[derive(Properties, Clone, PartialEq)]
pub struct DefaultCardpackPageProps {
    #[prop_or_default]
    pub app_state: AppStateHandle,
    #[prop_or_default]
    pub default_cardpack_name: String,
}

impl SharedState for DefaultCardpackPageProps {
    type Handle = SharedHandle<AppState>;
    fn handle(&mut self) -> &mut AppStateHandle {
        &mut self.app_state
    }
}

impl Component for DefaultCardpackPage {
    type Message = ();
    type Properties = DefaultCardpackPageProps;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props.neq_assign(props)
    }

    fn view(&self) -> Html {
        html! {
            <div>
                { format!("Welcome to the default cardpack page for {}!", self.props.default_cardpack_name) }
            </div>
        }
    }
}

pub type SharedStateDefaultCardpackPage = SharedStateComponent<DefaultCardpackPage>;
