mod current_game_page;
mod custom_cardpack_page;
mod default_cardpack_list_page;
mod default_cardpack_page;
mod game_list_page;
mod home_page;
mod login_page;
mod logout_page;
mod settings_page;
mod user_page;
use super::helper::{convert_to_async_option, AsyncOption};
use crate::components::nav_bar::NavBar;
use crate::proto::crusty_cards_api::*;
use crate::user_service::{get_current_user, get_current_user_settings};
use cards_frontend_shared::rpc::Status;
use current_game_page::SharedStateCurrentGamePage;
use custom_cardpack_page::SharedStateCustomCardpackPage;
use default_cardpack_list_page::SharedStateDefaultCardpackListPage;
use default_cardpack_page::SharedStateDefaultCardpackPage;
use game_list_page::SharedStateGameListPage;
use home_page::SharedStateHomePage;
use login_page::SharedStateLoginPage;
use logout_page::SharedStateLogoutPage;
use settings_page::SharedStateSettingsPage;
use std::cmp::PartialEq;
use user_page::SharedStateUserPage;
use yew::prelude::*;
use yew_router::agent::RouteRequest;
use yew_router::prelude::*;
use yew_services::fetch::FetchTask;
use yew_state::StateHandle;
use yew_state::{SharedHandle, SharedStateComponent};
use yewtil::NeqAssign;

#[derive(Switch, Debug, Clone)]
pub enum AppRoute {
    #[to = "/login"]
    LoginPage,
    #[to = "/logout"]
    LogoutPage,
    #[to = "/settings"]
    SettingsPage,
    #[to = "/users/{user}/cardpacks/{cardpack}"]
    CustomCardpackPage(String, String),
    #[to = "/defaultCardpacks"]
    DefaultCardpackListPage,
    #[to = "/defaultCardpacks/{cardpack}"]
    DefaultCardpackPage(String),
    #[to = "/users/{user}"]
    UserPage(String),
    #[to = "/gameList"]
    GameListPage,
    #[to = "/game"]
    CurrentGamePage,
    #[to = "/"]
    HomePage,
}

pub fn push_route(route: AppRoute) {
    let mut dispatcher: RouteAgentDispatcher<()> = RouteAgentDispatcher::new();
    dispatcher.send(RouteRequest::ChangeRoute(yew_router::route::Route::from(
        route,
    )));
}

pub fn replace_route(route: AppRoute) {
    let mut dispatcher: RouteAgentDispatcher<()> = RouteAgentDispatcher::new();
    dispatcher.send(RouteRequest::ReplaceRoute(yew_router::route::Route::from(
        route,
    )));
}

#[derive(Default, Clone, PartialEq)]
pub struct AppState {
    current_user: AsyncOption<User>,
    current_user_settings: AsyncOption<UserSettings>,
}

pub type AppStateHandle = SharedHandle<AppState>;

pub struct App {
    app_state: AppStateHandle,
    // This task handle is only used to prevent the request from being cancelled.
    #[allow(dead_code)]
    get_current_user_task: FetchTask,
    // This task handle is only used to prevent the request from being cancelled.
    #[allow(dead_code)]
    get_current_user_settings_task: FetchTask,
}

#[derive(Debug)]
pub enum AppMsg {
    UpdateCurrentUser(AsyncOption<User>),
    UpdateCurrentUserSettings(AsyncOption<UserSettings>),
}

impl Component for App {
    type Message = AppMsg;
    type Properties = AppStateHandle;

    fn create(app_state: Self::Properties, link: ComponentLink<Self>) -> Self {
        let get_current_user_task =
            get_current_user(link.callback_once(|res: Result<User, Status>| {
                AppMsg::UpdateCurrentUser(convert_to_async_option(res))
            }));
        let get_current_user_settings_task =
            get_current_user_settings(link.callback_once(|res: Result<UserSettings, Status>| {
                AppMsg::UpdateCurrentUserSettings(convert_to_async_option(res))
            }));
        Self {
            app_state,
            get_current_user_task,
            get_current_user_settings_task,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        yew_services::ConsoleService::log(&format!("{:?}", msg));
        match msg {
            AppMsg::UpdateCurrentUser(user_or) => self
                .app_state
                .reduce(|app_state| app_state.current_user = user_or),
            AppMsg::UpdateCurrentUserSettings(user_settings_or) => self
                .app_state
                .reduce(|app_state| app_state.current_user_settings = user_settings_or),
        };
        true
    }

    fn change(&mut self, app_state: Self::Properties) -> ShouldRender {
        self.app_state.neq_assign(app_state)
    }

    fn view(&self) -> Html {
        let render = Router::render(|route: AppRoute| match route {
            AppRoute::LoginPage => html! {<SharedStateLoginPage/>},
            AppRoute::LogoutPage => html! {<SharedStateLogoutPage/>},
            AppRoute::SettingsPage => html! {<SharedStateSettingsPage/>},
            AppRoute::UserPage(user) => {
                html! {<SharedStateUserPage user_name=format!("users/{}", user)/>}
            }
            AppRoute::CustomCardpackPage(user, cardpack) => {
                html! {<SharedStateCustomCardpackPage user_name=format!("users/{}", user) custom_cardpack_name=format!("users/{}/cardpacks/{}", user, cardpack)/>}
            }
            AppRoute::DefaultCardpackListPage => {
                html! {<SharedStateDefaultCardpackListPage/>}
            }
            AppRoute::DefaultCardpackPage(cardpack) => {
                html! {<SharedStateDefaultCardpackPage default_cardpack_name=format!("defaultCardpacks/{}", cardpack) />}
            }
            AppRoute::GameListPage => html! {<SharedStateGameListPage/>},
            AppRoute::CurrentGamePage => html! {<SharedStateCurrentGamePage/>},
            AppRoute::HomePage => html! {<SharedStateHomePage/>},
        });

        html! {
            <>
                <style>
                    {
                        // The loading spinner in the top app bar is grey, instead of defaulting to the same color as the app bar itself.
                        "mwc-top-app-bar mwc-circular-progress {
                            --mdc-theme-primary: grey;
                        }"
                    }
                </style>
                <NavBar current_user=self.app_state.state().current_user.clone()>
                    <Router<AppRoute, ()> render=render/>
                </NavBar>
            </>
        }
    }
}

pub type SharedStateApp = SharedStateComponent<App>;
