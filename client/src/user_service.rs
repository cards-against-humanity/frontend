use crate::helper::{make_api_request, make_api_request_without_body};
use crate::proto::crusty_cards_api::*;
use cards_frontend_shared::rpc::Status;
use yew::Callback;
use yew_services::fetch::FetchTask;

pub fn get_current_user(callback: Callback<Result<User, Status>>) -> FetchTask {
    make_api_request_without_body("/api/user/me", callback)
}

pub fn get_current_user_settings(callback: Callback<Result<UserSettings, Status>>) -> FetchTask {
    make_api_request_without_body("/api/userSettings/me", callback)
}

pub fn get_user(name: String, callback: Callback<Result<User, Status>>) -> FetchTask {
    make_api_request(
        "/api/userService/getUser",
        GetUserRequest { name },
        callback,
    )
}
