mod proto {
    pub mod google {
        pub mod api {
            include!("proto/google.api.rs");
        }

        pub mod protobuf {
            include!("proto/google.protobuf.rs");
        }
    }

    pub mod crusty_cards_api {
        include!("proto/crusty_cards_api.rs");
    }
}

mod components;
mod helper;
mod pages;
mod user_service;

use wasm_bindgen::prelude::*;
use yew::prelude::*;

#[wasm_bindgen(start)]
pub fn main() {
    App::<pages::SharedStateApp>::new().mount_to_body();
}
