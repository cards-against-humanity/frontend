use cards_frontend_shared::rpc::{
    binary_decode_prost_message_or_status, binary_encode_prost_message, Code, Status,
};
use serde::de::DeserializeOwned;
use yew::format::Binary;
use yew::Callback;
use yew_services::fetch::Request;
use yew_services::fetch::{FetchService, FetchTask};

type FetchCallback<Res> = Callback<Result<Res, Status>>;

pub fn make_api_request<
    Req: prost::Message,
    Res: 'static + prost::Message + DeserializeOwned + Default,
>(
    path: &str,
    request: Req,
    callback: FetchCallback<Res>,
) -> FetchTask {
    let decoder_callback = callback.reform(|http_res: yew_services::fetch::Response<Binary>| {
        let http_body = http_res.into_body().unwrap();
        binary_decode_prost_message_or_status(&http_body)
    });
    let http_req: Request<Binary> = Request::post(path)
        .body(Ok(binary_encode_prost_message(request)))
        .unwrap();
    FetchService::fetch_binary(http_req, decoder_callback).unwrap()
}

pub fn make_api_request_without_body<Res: 'static + prost::Message + DeserializeOwned + Default>(
    path: &str,
    callback: FetchCallback<Res>,
) -> FetchTask {
    let decoder_callback = callback.reform(|http_res: yew_services::fetch::Response<Binary>| {
        let http_body = http_res.into_body().unwrap();
        binary_decode_prost_message_or_status(&http_body)
    });
    let http_req: Request<Binary> = Request::post(path).body(Ok(Vec::new())).unwrap();
    FetchService::fetch_binary(http_req, decoder_callback).unwrap()
}

pub fn convert_to_async_option<Val>(val_or: Result<Val, Status>) -> AsyncOption<Val> {
    match val_or {
        Ok(val) => AsyncOption::Some(val),
        Err(err) => match err.code() {
            Code::NotFound => AsyncOption::None,
            _ => AsyncOption::Error,
        },
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum AsyncOption<T> {
    Loading,
    Error,
    None,
    Some(T),
}

impl<T> Default for AsyncOption<T> {
    fn default() -> Self {
        Self::Loading
    }
}
