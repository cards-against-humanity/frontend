use std::fs;

fn main() -> Result<(), std::io::Error> {
    let _ = fs::create_dir("./src/proto");

    let mut prost_config = prost_build::Config::new();
    prost_config.type_attribute(".", "#[derive(serde::Serialize, serde::Deserialize)]");
    prost_config.type_attribute(".", "#[serde(rename_all = \"camelCase\")]");
    prost_config.compile_well_known_types();
    prost_config.out_dir("./src/proto");
    prost_config.compile_protos(
        &[
            "../cards-proto/crusty_cards_api/cardpack_service.proto",
            "../cards-proto/crusty_cards_api/model.proto",
            "../cards-proto/crusty_cards_api/user_service.proto",
            "../cards-proto/crusty_cards_api/game_service.proto",
        ],
        &["../cards-proto"],
    )
}
