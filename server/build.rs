// use serde::Serialize;
// use std::fs;

// TODO - Uncomment this code and follow the TODO in the main gitignore file.
fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let _ = fs::create_dir("./src/proto");

    // let mut config = prost_build::Config::new();
    // config.compile_well_known_types();

    // tonic_build::configure()
    //     .out_dir("./src/proto")
    //     .build_server(false)
    //     .build_client(true)
    //     .format(true)
    //     .type_attribute(".", "#[derive(serde::Serialize, serde::Deserialize)] #[serde(rename_all = \"snake_case\")]")
    //     .compile_with_config(
    //         config,
    //         &[
    //             "../cards-proto/crusty_cards_api/cardpack_service.proto",
    //             "../cards-proto/crusty_cards_api/model.proto",
    //             "../cards-proto/crusty_cards_api/user_service.proto",
    //             "../cards-proto/crusty_cards_api/game_service.proto",
    //         ],
    //         &["../cards-proto"],
    //     )?;

    Ok(())
}
