use hmac::{Hmac, NewMac};
use jwt::{SignWithKey, VerifyWithKey};
use sha2::Sha256;
use std::collections::HashMap;

pub struct JWTParser {
    key: Hmac<Sha256>,
}

// TODO - Put an expiry time on encoded JWTs.
impl JWTParser {
    pub fn new(secret: &str) -> Self {
        Self {
            key: Hmac::new_varkey(secret.as_bytes()).unwrap(),
        }
    }

    pub fn encode_jwt(&self, user_name: &str) -> String {
        let mut claims = HashMap::new();
        claims.insert("name", user_name);
        claims.sign_with_key(&self.key).unwrap()
    }

    pub fn decode_jwt(&self, jwt: &str) -> String {
        let mut claims: HashMap<String, String> = jwt.verify_with_key(&self.key).unwrap();
        claims.remove("name").unwrap()
    }
}
