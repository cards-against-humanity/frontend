/// Top-level resource representing a user.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct User {
    /// The resource name of the user.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The display name of the user.
    #[prost(string, tag = "2")]
    pub display_name: std::string::String,
    /// When the user was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// When the user was last modified.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
/// Singleton resource belonging to a user.
/// Contains bytes representing an image.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserProfileImage {
    /// The resource name of the user profile image.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// Image data. Empty by default for new users.
    #[prost(bytes, tag = "2")]
    pub image_data: std::vec::Vec<u8>,
}
/// Configuration settings used to create a game.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GameConfig {
    /// The displayed name of the game.
    /// Does not have to be unique among all
    /// games since each game has a unique game_id.
    #[prost(string, tag = "1")]
    pub display_name: std::string::String,
    /// The maximum number of players that can be in the game at once.
    /// Artificial players do not count towards this limit.
    /// Value must be at least 3 and at most 100.
    #[prost(int32, tag = "2")]
    pub max_players: i32,
    /// The hand limit of each player.
    #[prost(int32, tag = "5")]
    pub hand_size: i32,
    /// A list of custom cardpacks that should be used when making the game.
    /// All cards from all custom cardpacks listed will be pulled from
    /// the main api at game creation.
    #[prost(string, repeated, tag = "6")]
    pub custom_cardpack_names: ::std::vec::Vec<std::string::String>,
    /// A list of default cardpacks that should be used when making the game.
    /// All cards from all default cardpacks listed will be pulled from
    /// the main api at game creation.
    #[prost(string, repeated, tag = "7")]
    pub default_cardpack_names: ::std::vec::Vec<std::string::String>,
    /// Defines how blank white cards behave in-game
    /// and how many should be added to the deck.
    #[prost(message, optional, tag = "8")]
    pub blank_white_card_config: ::std::option::Option<game_config::BlankWhiteCardConfig>,
    /// The condition through which
    /// a game will automatically end.
    #[prost(oneof = "game_config::EndCondition", tags = "3, 4")]
    pub end_condition: ::std::option::Option<game_config::EndCondition>,
}
pub mod game_config {
    /// Settings defining how blank white cards behave in-game.
    #[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub struct BlankWhiteCardConfig {
        /// Specifies behavior of blank white cards.
        #[prost(enumeration = "blank_white_card_config::Behavior", tag = "1")]
        pub behavior: i32,
        #[prost(oneof = "blank_white_card_config::BlankWhiteCardsAdded", tags = "2, 3")]
        pub blank_white_cards_added:
            ::std::option::Option<blank_white_card_config::BlankWhiteCardsAdded>,
    }
    pub mod blank_white_card_config {
        /// How blank white cards can be used.
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        #[derive(serde::Serialize, serde::Deserialize)]
        #[serde(rename_all = "snake_case")]
        pub enum Behavior {
            /// Default value. This value is unused.
            Unspecified = 0,
            /// No blank white cards are added to the deck.
            Disabled = 1,
            /// Allow users to play blank white cards from their hand
            /// and write any text (limited to 100 characters).
            OpenText = 2,
            /// Similar to OPEN_TEXT, except the card text must exactly match
            /// another white card that is in the current game. To search for
            /// cards to copy, use the ListWhiteCardTexts rpc.
            DuplicateText = 3,
        }
        #[derive(Clone, PartialEq, ::prost::Oneof, serde::Serialize, serde::Deserialize)]
        #[serde(rename_all = "snake_case")]
        pub enum BlankWhiteCardsAdded {
            /// Adds a fixed number of blank white cards to the deck.
            #[prost(int32, tag = "2")]
            CardCount(i32),
            /// A number between 0.0 and 0.8 specifying what percentage of the total
            /// deck size should be blank white cards. For example, if there are 900
            /// non-blank white cards in your game and you specify a percentage
            /// of 0.1, there will be 100 blank white cards added for a total of 1000.
            ///
            /// The upper limit is 0.8 because, as this value approaches 1.0, the
            /// number of blank white cards needed increases exponentially. A value of 0.5
            /// doubles the size of the deck, a value of 0.8 increases the size by 5x
            /// and a value of 0.99 would increase it by 100x.
            #[prost(double, tag = "3")]
            Percentage(f64),
        }
    }
    /// The condition through which
    /// a game will automatically end.
    #[derive(Clone, PartialEq, ::prost::Oneof, serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum EndCondition {
        /// Once a player reaches this score, the game
        /// is stopped automatically and that player wins.
        #[prost(int32, tag = "3")]
        MaxScore(i32),
        /// The game never ends automatically.
        /// Scores are still tracked.
        #[prost(bool, tag = "4")]
        EndlessMode(bool),
    }
}
/// Singleton resource belonging to a user. Contains user-specific settings.
/// Exactly one exists for every user, and are created automatically whenever a user is created.
/// All values are unset for new users and can only be changed by calling UpdateUserSettings.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserSettings {
    /// The resource name of the user settings.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The color scheme to show for the user.
    #[prost(enumeration = "user_settings::ColorScheme", tag = "2")]
    pub color_scheme: i32,
    /// Default game settings for a user to quickly start a game.
    #[prost(message, optional, tag = "3")]
    pub quick_start_game_config: ::std::option::Option<GameConfig>,
}
pub mod user_settings {
    /// Color schemes the frontend supports.
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    #[derive(serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum ColorScheme {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Default color scheme for new users.
        DefaultLight = 1,
        /// Dark mode.
        DefaultDark = 2,
    }
}
/// Resource belonging to a user. Represents a collection
/// of related cards that can be used when creating games.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CustomCardpack {
    /// The resource name of the custom cardpack.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The display name of the custom cardpack.
    #[prost(string, tag = "2")]
    pub display_name: std::string::String,
    /// When the custom cardpack was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// This field is reset anytime any changes are made to the custom cardpack including adding, modifying
    /// and deleting cards as well as renaming the custom cardpack.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// If this field is not empty, it indicates that the custom cardpack has been deleted. CustomCardpacks that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "5")]
    pub delete_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
/// A black card. Behaves like a black card in Cards Against Humanity.
/// Belongs to a CustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CustomBlackCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The content of the card. Limited to 300 characters.
    /// Whitespace at the beginning and end is automatically trimmed when this field is set or modified.
    #[prost(string, tag = "2")]
    pub text: std::string::String,
    /// The number of white cards that should be played when this card is used.
    /// Value must be 1, 2, or 3.
    #[prost(int32, tag = "3")]
    pub answer_fields: i32,
    /// When the card was created.
    #[prost(message, optional, tag = "4")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// When the card was last modified.
    #[prost(message, optional, tag = "5")]
    pub update_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// If this field is not empty, it indicates that the card has been deleted. Cards that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "6")]
    pub delete_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
/// A white card. Behaves like a white card in Cards Against Humanity.
/// Belongs to a CustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CustomWhiteCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The content of the card. Limited to 100 characters.
    /// Whitespace at the beginning and end is automatically trimmed when this field is set or modified.
    #[prost(string, tag = "2")]
    pub text: std::string::String,
    /// When the card was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// When the card was last modified.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// If this field is not empty, it indicates that the card has been deleted. Cards that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "5")]
    pub delete_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
/// Represents a collection of related cards that can be used when creating games.
/// Different from a custom cardpack because this is a top-level resource and therefore
/// has no owner, and it is immutable along with all cards in it.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DefaultCardpack {
    /// The resource name of the default cardpack.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The display name of the default cardpack.
    #[prost(string, tag = "2")]
    pub display_name: std::string::String,
}
/// An immutable default black card. Behaves like a black card in Cards Against Humanity.
/// Belongs to a DefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DefaultBlackCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The content of the card.
    #[prost(string, tag = "2")]
    pub text: std::string::String,
    /// The number of white cards that should be played when this card is used.
    /// Value must be 1, 2, or 3.
    #[prost(int32, tag = "3")]
    pub answer_fields: i32,
}
/// An immutable default white card. Behaves like a white card in Cards Against Humanity.
/// Belongs to a DefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DefaultWhiteCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: std::string::String,
    /// The content of the card.
    #[prost(string, tag = "2")]
    pub text: std::string::String,
}
/// Request message for CreateCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CreateCustomCardpackRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The custom cardpack to create.
    #[prost(message, optional, tag = "2")]
    pub custom_cardpack: ::std::option::Option<CustomCardpack>,
}
/// Request message for GetCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetCustomCardpackRequest {
    /// The name of the custom cardpack to retrieve.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for ListCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomCardpacksRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
    /// Show soft-deleted custom cardpacks instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomCardpacksResponse {
    /// The requested custom cardpacks.
    #[prost(message, repeated, tag = "1")]
    pub custom_cardpacks: ::std::vec::Vec<CustomCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UpdateCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateCustomCardpackRequest {
    /// The custom cardpack to update.
    #[prost(message, optional, tag = "1")]
    pub custom_cardpack: ::std::option::Option<CustomCardpack>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for DeleteCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DeleteCustomCardpackRequest {
    /// The name of the custom cardpack to delete.
    /// Deleted custom cardpacks are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for CreateCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CreateCustomBlackCardRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The black card to create.
    #[prost(message, optional, tag = "2")]
    pub custom_black_card: ::std::option::Option<CustomBlackCard>,
}
/// Request message for CreateCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CreateCustomWhiteCardRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The white card to create.
    #[prost(message, optional, tag = "2")]
    pub custom_white_card: ::std::option::Option<CustomWhiteCard>,
}
/// Request message for ListCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomBlackCardsRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
    /// Show soft-deleted cards instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomBlackCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub custom_black_cards: ::std::vec::Vec<CustomBlackCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomWhiteCardsRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
    /// Show soft-deleted cards instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListCustomWhiteCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub custom_white_cards: ::std::vec::Vec<CustomWhiteCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UpdateCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateCustomBlackCardRequest {
    /// The card to update.
    #[prost(message, optional, tag = "1")]
    pub custom_black_card: ::std::option::Option<CustomBlackCard>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for UpdateCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateCustomWhiteCardRequest {
    /// The card to update.
    #[prost(message, optional, tag = "1")]
    pub custom_white_card: ::std::option::Option<CustomWhiteCard>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for DeleteCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DeleteCustomBlackCardRequest {
    /// The name of the black card to delete.
    /// Deleted black cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/blackCards/{custom_black_card}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for DeleteCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DeleteCustomWhiteCardRequest {
    /// The name of the white card to delete.
    /// Deleted white cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/whiteCards/{custom_white_card}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for BatchCreateCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BatchCreateCustomBlackCardsRequest {
    /// The parent resource shared by all cards being created.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    /// The `parent` field in all nested request messages
    /// must either be empty or match this field.
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The request message specifying the resources to create.
    /// A maximum of 10000 cards can be created in a batch.
    #[prost(message, repeated, tag = "2")]
    pub requests: ::std::vec::Vec<CreateCustomBlackCardRequest>,
}
/// Response message for BatchCreateCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BatchCreateCustomBlackCardsResponse {
    /// Black cards created. Guaranteed to be in the same order as the request.
    #[prost(message, repeated, tag = "1")]
    pub custom_black_cards: ::std::vec::Vec<CustomBlackCard>,
}
/// Request message for BatchCreateCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BatchCreateCustomWhiteCardsRequest {
    /// The parent resource shared by all cards being created.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    /// The `parent` field in all nested request messages
    /// must either be empty or match this field.
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The request message specifying the resources to create.
    /// A maximum of 10000 cards can be created in a batch.
    #[prost(message, repeated, tag = "2")]
    pub requests: ::std::vec::Vec<CreateCustomWhiteCardRequest>,
}
/// Response message for BatchCreateCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BatchCreateCustomWhiteCardsResponse {
    /// White cards created. Guaranteed to be in the same order as the request.
    #[prost(message, repeated, tag = "1")]
    pub custom_white_cards: ::std::vec::Vec<CustomWhiteCard>,
}
/// Request message for GetDefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetDefaultCardpackRequest {
    /// The name of the default cardpack to retrieve.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for ListDefaultCardpacks. Items are not sorted by any particular field, but rather by importance (i.e. base packs are first, then large expansion packs, etc.).
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultCardpacksRequest {
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "1")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "2")]
    pub page_token: std::string::String,
}
/// Response message for ListDefaultCardpacks.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultCardpacksResponse {
    /// The requested default cardpacks.
    #[prost(message, repeated, tag = "1")]
    pub default_cardpacks: ::std::vec::Vec<DefaultCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListDefaultBlackCards. Items are sorted by `text`.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultBlackCardsRequest {
    /// The parent default cardpack.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
}
/// Response message for ListDefaultBlackCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultBlackCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub default_black_cards: ::std::vec::Vec<DefaultBlackCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListDefaultWhiteCards. Items are sorted by `text`.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultWhiteCardsRequest {
    /// The parent default cardpack.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
}
/// Response message for ListDefaultWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListDefaultWhiteCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub default_white_cards: ::std::vec::Vec<DefaultWhiteCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UndeleteCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UndeleteCustomCardpackRequest {
    /// The name of the custom cardpack to undelete.
    /// Deleted custom cardpacks are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for UndeleteCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UndeleteCustomBlackCardRequest {
    /// The name of the black card to undelete.
    /// Deleted black cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/blackCards/{custom_black_card}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for UndeleteCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UndeleteCustomWhiteCardRequest {
    /// The name of the white card to undelete.
    /// Deleted white cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/whiteCards/{custom_white_card}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for ListFavoritedCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListFavoritedCustomCardpacksRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
}
/// Response message for ListFavoritedCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListFavoritedCustomCardpacksResponse {
    /// The requested custom cardpacks.
    /// The user's favorite list contains references to cardpacks, so if
    /// a cardpack in the list is deleted, the reference still exists in
    /// the user's favorites. When this occurs, a completely empty proto
    /// is included in this repeated field. Also, soft-deleted cardpacks
    /// do still show up in this list - an empty proto is only used when
    /// the cardpack has been permanently deleted.
    #[prost(message, repeated, tag = "1")]
    pub custom_cardpacks: ::std::vec::Vec<CustomCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
}
/// Request message for LikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct LikeCustomCardpackRequest {
    /// The user who is liking the custom cardpack.
    #[prost(string, tag = "1")]
    pub user: std::string::String,
    /// The custom cardpack they are liking.
    #[prost(string, tag = "2")]
    pub custom_cardpack: std::string::String,
}
/// Request message for UnlikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UnlikeCustomCardpackRequest {
    /// The user who is unliking the custom cardpack.
    #[prost(string, tag = "1")]
    pub user: std::string::String,
    /// The custom cardpack they are unliking.
    #[prost(string, tag = "2")]
    pub custom_cardpack: std::string::String,
}
/// Request message for CheckDoesUserLikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CheckDoesUserLikeCustomCardpackRequest {
    /// The user to check.
    #[prost(string, tag = "1")]
    pub user: std::string::String,
    /// The custom cardpack to check against.
    #[prost(string, tag = "2")]
    pub custom_cardpack: std::string::String,
}
/// Response message for CheckDoesUserLikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CheckDoesUserLikeCustomCardpackResponse {
    /// Whether the user has liked the custom cardpack.
    #[prost(bool, tag = "1")]
    pub is_liked: bool,
}
#[doc = r" Generated client implementations."]
pub mod cardpack_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = " Enables users to create cardpacks containing black and white cards."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - DefaultCardpack: Top-level resource, contain sets of cards that can be used in games"]
    #[doc = " - DefaultBlackCard: Belong to a DefaultCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - DefaultWhiteCard: Belong to a DefaultCardpack and act like a white card from Cards Against Humanity"]
    #[doc = " - CustomCardpack: Belong to a user, contain sets of cards that can be used in games"]
    #[doc = " - CustomBlackCard: Belong to a CustomCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - CustomWhiteCard: Belong to a CustomCardpack and act like a white card from Cards Against Humanity"]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    pub struct CardpackServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl CardpackServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> CardpackServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        #[doc = " Creates a CustomCardpack."]
        pub async fn create_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a CustomCardpack."]
        pub async fn get_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::GetCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/GetCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomCardpacks."]
        pub async fn list_custom_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListCustomCardpacksResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomCardpack."]
        pub async fn update_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomCardpack. Deleted CustomCardpacks can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomCardpacks cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a CustomBlackCard."]
        pub async fn create_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a CustomWhiteCard."]
        pub async fn create_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomBlackCards."]
        pub async fn list_custom_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomBlackCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomWhiteCards."]
        pub async fn list_custom_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomWhiteCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomBlackCard."]
        pub async fn update_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomWhiteCard."]
        pub async fn update_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomBlackCard. Deleted CustomBlackCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomBlackCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomWhiteCard. Deleted CustomWhiteCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomWhiteCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a batch of CustomBlackCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        pub async fn batch_create_custom_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::BatchCreateCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomBlackCardsResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/BatchCreateCustomBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a batch of CustomWhiteCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        pub async fn batch_create_custom_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::BatchCreateCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomWhiteCardsResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/BatchCreateCustomWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a DefaultCardpack."]
        pub async fn get_default_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::GetDefaultCardpackRequest>,
        ) -> Result<tonic::Response<super::DefaultCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/GetDefaultCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultCardpacks."]
        pub async fn list_default_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListDefaultCardpacksResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultBlackCards."]
        pub async fn list_default_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultBlackCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultWhiteCards."]
        pub async fn list_default_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultWhiteCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomCardpack, returning it to its previous state."]
        #[doc = " Deleted CustomCardpacks can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomCardpacks, use"]
        #[doc = " the ListCustomCardpacks rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomBlackCard, returning it to its previous state."]
        #[doc = " Deleted CustomBlackCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomBlackCards, use"]
        #[doc = " the ListCustomBlackCards rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomWhiteCard, returning it to its previous state."]
        #[doc = " Deleted CustomWhiteCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomWhiteCards, use"]
        #[doc = " the ListCustomWhiteCards rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn list_favorited_custom_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListFavoritedCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListFavoritedCustomCardpacksResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListFavoritedCustomCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Adds CustomCardpack to a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        pub async fn like_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::LikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::super::google::protobuf::Empty>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/LikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Removes CustomCardpack from a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        pub async fn unlike_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UnlikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::super::google::protobuf::Empty>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UnlikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns whether a User has added a CustomCardpack to their favorites."]
        pub async fn check_does_user_like_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::CheckDoesUserLikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CheckDoesUserLikeCustomCardpackResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CheckDoesUserLikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for CardpackServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for CardpackServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "CardpackServiceClient {{ ... }}")
        }
    }
}
/// Used to identify a user through their oauth account.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct OAuthCredentials {
    /// The user's oauth provider. Currently Google is the only provider that's used.
    #[prost(string, tag = "1")]
    pub oauth_provider: std::string::String,
    /// The oauth id of the user from the given provider.
    #[prost(string, tag = "2")]
    pub oauth_id: std::string::String,
}
/// Request message for GetUser.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetUserRequest {
    /// The name of the user to retrieve.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for UpdateUser.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateUserRequest {
    /// The user to update.
    #[prost(message, optional, tag = "1")]
    pub user: ::std::option::Option<User>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for GetUserSettings.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetUserSettingsRequest {
    /// The name of the user settings to retrieve.
    /// Format: users/{user}/settings
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for UpdateUserSettings.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateUserSettingsRequest {
    /// The user settings to update.
    #[prost(message, optional, tag = "1")]
    pub user_settings: ::std::option::Option<UserSettings>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for GetUserProfileImage.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetUserProfileImageRequest {
    /// The name of the profile image to retrieve.
    /// Format: users/{user}/profileImage
    #[prost(string, tag = "1")]
    pub name: std::string::String,
}
/// Request message for UpdateUserProfileImage.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UpdateUserProfileImageRequest {
    /// The profile image to update.
    #[prost(message, optional, tag = "1")]
    pub user_profile_image: ::std::option::Option<UserProfileImage>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::std::option::Option<super::google::protobuf::FieldMask>,
}
/// Request message for GetOrCreateUser.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetOrCreateUserRequest {
    /// The user's oauth credentials.
    #[prost(message, optional, tag = "1")]
    pub oauth_credentials: ::std::option::Option<OAuthCredentials>,
    /// Used to create the user if it does not exist yet.
    #[prost(message, optional, tag = "2")]
    pub user: ::std::option::Option<User>,
}
/// Request message for UserSearch.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserSearchRequest {
    /// The search query to suggest results from.
    #[prost(string, tag = "1")]
    pub query: std::string::String,
}
/// Response message for UserSearch.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UserSearchResponse {
    /// Search results.
    #[prost(message, repeated, tag = "1")]
    pub users: ::std::vec::Vec<User>,
}
/// Request message for AutocompleteUserSearch.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct AutocompleteUserSearchRequest {
    /// The search query to suggest results from.
    #[prost(string, tag = "1")]
    pub query: std::string::String,
}
/// Response message for AutocompleteUserSearch.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct AutocompleteUserSearchResponse {
    /// List of autocomplete suggestions.
    /// Each entry is guaranteed to contain
    /// the original query.
    #[prost(string, repeated, tag = "1")]
    pub autocomplete_entries: ::std::vec::Vec<std::string::String>,
}
#[doc = r" Generated client implementations."]
pub mod user_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = " Enables creating and retrieval of users and user data."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - User: Pretty much what you'd expect."]
    #[doc = " - UserSettings: A user's account settings and preferences."]
    #[doc = " - UserProfileImage: A user's profile picture."]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    pub struct UserServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl UserServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> UserServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        #[doc = " Returns a User."]
        pub async fn get_user(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/GetUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User."]
        pub async fn update_user(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/UpdateUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a User's settings."]
        pub async fn get_user_settings(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetUserSettings",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User's settings."]
        pub async fn update_user_settings(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/UpdateUserSettings",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a User's profile image."]
        pub async fn get_user_profile_image(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetUserProfileImage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User's profile image."]
        pub async fn update_user_profile_image(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/UpdateUserProfileImage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Gets a user, or creates one if it does not exist."]
        #[doc = " This method of creating users may seem strange. The reason we have a 'get-or-create' instead of"]
        #[doc = " a simpler 'create' rpc is because of the way that oauth login works. When someone logs in using"]
        #[doc = " oauth, the oauth provider doesn't know whether they already have an account here (i.e. a User"]
        #[doc = " exists that matches their oauth account). So at login time, we need to treat every user the same"]
        #[doc = " regardless of whether they've signed in before since we can't know that ahead of time. With the"]
        #[doc = " get-or-create pattern we provide all data we might need if a new User needs to be created, and"]
        #[doc = " ignore it if the User already exists."]
        pub async fn get_or_create_user(
            &mut self,
            request: impl tonic::IntoRequest<super::GetOrCreateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetOrCreateUser",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Fetches multiple Users based off search query."]
        pub async fn user_search(
            &mut self,
            request: impl tonic::IntoRequest<super::UserSearchRequest>,
        ) -> Result<tonic::Response<super::UserSearchResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/UserSearch");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Fetches search suggestions that are used to provide autocomplete functionality."]
        pub async fn autocomplete_user_search(
            &mut self,
            request: impl tonic::IntoRequest<super::AutocompleteUserSearchRequest>,
        ) -> Result<tonic::Response<super::AutocompleteUserSearchResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/AutocompleteUserSearch",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for UserServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for UserServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "UserServiceClient {{ ... }}")
        }
    }
}
/// Returns all games that match the search criteria.
/// Has no pagination, simply returns the entire list.
/// Results are always ordered by create_time.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct SearchGamesRequest {
    /// Filters games by name, i.e. games must contain the
    /// exact text of the query in their name to match.
    #[prost(string, tag = "1")]
    pub query: std::string::String,
    /// The minimum number of slots games must have available. Used to filter out games that are full
    /// or nearly full. Value cannot be negative.
    #[prost(int32, tag = "2")]
    pub min_available_player_slots: i32,
    #[prost(enumeration = "search_games_request::GameStageFilter", tag = "3")]
    pub game_stage_filter: i32,
}
pub mod search_games_request {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    #[derive(serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum GameStageFilter {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Bypass game stage filter.
        FilterNone = 1,
        /// Filter all non-running games.
        FilterStopped = 2,
        /// Filter all running games.
        FilterRunning = 3,
    }
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct SearchGamesResponse {
    #[prost(message, repeated, tag = "1")]
    pub games: ::std::vec::Vec<GameInfo>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CreateGameRequest {
    /// The name of the user creating the game.
    /// This user will be the owner once the game is created.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// Game configuration options.
    #[prost(message, optional, tag = "2")]
    pub game_config: ::std::option::Option<GameConfig>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct StartGameRequest {
    /// The name of the user trying to start the game.
    /// Must be the name of someone who owns the game that they are in.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct StopGameRequest {
    /// The name of the user trying to stop the game.
    /// Must be the name of someone who owns the game that they are in.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct JoinGameRequest {
    /// The user who is joining a game.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The unique identifier of the game to join.
    #[prost(string, tag = "2")]
    pub game_id: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct LeaveGameRequest {
    /// The user who is leaving a game.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct KickUserRequest {
    /// The user who is kicking another user.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The user being kicked.
    #[prost(string, tag = "2")]
    pub troll_user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BanUserRequest {
    /// The user who is banning another user.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The user being banned.
    #[prost(string, tag = "2")]
    pub troll_user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UnbanUserRequest {
    /// The user who is unbanning another user.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The user being unbanned.
    #[prost(string, tag = "2")]
    pub troll_user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct PlayCardsRequest {
    /// The user who is playing cards for the current round.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The cards to play. All cards must be in the user's hand or the request will fail.
    /// For each card in this list, if the `custom_white_card` property is present
    /// then only `custom_white_card.name` is required and all other fields are ignored.
    /// If the `default_white_card` property is present
    /// then only `default_white_card.name` is required and all other fields are ignored.
    /// If the `blank_white_card` property is present then `blank_white_card.id` and `blank_white_card.open_text` are required.
    /// The `blank_white_card.open_text` field should contain user-specified open text from a blank white card.
    /// The `blank_white_card` property is only allowed for games that have OPEN_TEXT behavior for blank white cards.
    #[prost(message, repeated, tag = "2")]
    pub cards: ::std::vec::Vec<PlayableWhiteCard>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct UnplayCardsRequest {
    /// The user who is unplaying their cards for the current round.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct VoteCardRequest {
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The 1-based index of which set of cards to vote for as the judge.
    #[prost(int32, tag = "2")]
    pub choice: i32,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct VoteStartNextRoundRequest {
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct AddArtificialPlayerRequest {
    /// The user who is adding an artificial player.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The display name of the new artificial player.
    /// A name is chosen automatically if it isn't provided here.
    #[prost(string, tag = "2")]
    pub display_name: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct RemoveArtificialPlayerRequest {
    /// The user who is removing an artificial player.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The artificial player to remove.
    /// If unspecified, removes the first artificial player, prioritizing queued players.
    #[prost(string, tag = "2")]
    pub artificial_player_id: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CreateChatMessageRequest {
    /// The name of the user creating the chat message.
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
    /// The chat message to create.
    #[prost(message, optional, tag = "2")]
    pub chat_message: ::std::option::Option<ChatMessage>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GetGameViewRequest {
    #[prost(string, tag = "1")]
    pub user_name: std::string::String,
}
/// Used to search among all cards that are in a game - including the dicard pile and all players' hands.
/// The purpose of this rpc is for games that use DUPLICATE_TEXT blank white cards.
/// Returned items are sorted alphabetically.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListWhiteCardTextsRequest {
    /// The game to search within.
    #[prost(string, tag = "1")]
    pub game_id: std::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: std::string::String,
    /// Filter cards by their text. Right now this does not have any
    /// special functionality described in https://google.aip.dev/160.
    /// It is strictly an exact text filter.
    #[prost(string, tag = "4")]
    pub filter: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ListWhiteCardTextsResponse {
    /// The text of each card.
    #[prost(string, repeated, tag = "1")]
    pub card_texts: ::std::vec::Vec<std::string::String>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: std::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ChatMessage {
    /// The user who sent the message.
    #[prost(message, optional, tag = "1")]
    pub user: ::std::option::Option<User>,
    /// The contents of the message. Cannot be blank.
    #[prost(string, tag = "2")]
    pub text: std::string::String,
    /// When the message was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ArtificialUser {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub id: std::string::String,
    /// The displayed name of the artificial user.
    /// Since each artificial user has a unique id,
    /// the name does not have to be unique.
    #[prost(string, tag = "2")]
    pub display_name: std::string::String,
}
/// Represents a real user or an artificial player in a game.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Player {
    /// The number of rounds this player has won.
    #[prost(int32, tag = "3")]
    pub score: i32,
    /// When the player was added to the game. For real users this is
    /// when the JoinGame RPC is called. For artificial players
    /// this is when the AddArtificialPlayer RPC is called.
    #[prost(message, optional, tag = "4")]
    pub join_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    #[prost(oneof = "player::Identifier", tags = "1, 2")]
    pub identifier: ::std::option::Option<player::Identifier>,
}
pub mod player {
    #[derive(Clone, PartialEq, ::prost::Oneof, serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum Identifier {
        #[prost(message, tag = "1")]
        User(super::User),
        #[prost(message, tag = "2")]
        ArtificialUser(super::ArtificialUser),
    }
}
/// Can be one of several different types of black cards.
/// This proto is used during gameplay to represent any type of black card.
/// The black card equivalent of the PlayableWhiteCard message.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BlackCardInRound {
    #[prost(oneof = "black_card_in_round::Card", tags = "1, 2")]
    pub card: ::std::option::Option<black_card_in_round::Card>,
}
pub mod black_card_in_round {
    #[derive(Clone, PartialEq, ::prost::Oneof, serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum Card {
        #[prost(message, tag = "1")]
        CustomBlackCard(super::CustomBlackCard),
        #[prost(message, tag = "2")]
        DefaultBlackCard(super::DefaultBlackCard),
    }
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct BlankWhiteCard {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub id: std::string::String,
    /// Text entered by the user (limited to 100 characters).
    /// Always empty for cards in a player's hand and
    /// always non-empty for cards that have been played.
    /// Required when playing a blank white card using the PlayCards rpc.
    #[prost(string, tag = "2")]
    pub open_text: std::string::String,
}
/// Can be one of several different types of white cards.
/// This proto is used during gameplay to represent any type of white card.
/// The white card equivalent of the BlackCardInRound message.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct PlayableWhiteCard {
    #[prost(oneof = "playable_white_card::Card", tags = "1, 2, 3")]
    pub card: ::std::option::Option<playable_white_card::Card>,
}
pub mod playable_white_card {
    #[derive(Clone, PartialEq, ::prost::Oneof, serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum Card {
        #[prost(message, tag = "1")]
        CustomWhiteCard(super::CustomWhiteCard),
        #[prost(message, tag = "2")]
        BlankWhiteCard(super::BlankWhiteCard),
        #[prost(message, tag = "3")]
        DefaultWhiteCard(super::DefaultWhiteCard),
    }
}
/// Contains cards played by
/// a single user for one round.
/// See the description for GameView.white_played
/// for details on how this is used
/// and when certain fields are filled.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct WhiteCardsPlayed {
    /// The player who played the cards.
    #[prost(message, optional, tag = "1")]
    pub player: ::std::option::Option<Player>,
    /// The text of the cards played by the user.
    /// We're including only the card text to hide
    /// whether it includes any blank white cards.
    /// Since artificial players never draw blank
    /// white cards, including full card protos
    /// could give away that an answer belongs to
    /// a real user if it contained blank white
    /// cards. By including only the text, even
    /// users with direct access to this api are
    /// unable to tell where it came from.
    #[prost(string, repeated, tag = "2")]
    pub card_texts: ::std::vec::Vec<std::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct PastRound {
    /// The black card that was used for the round.
    #[prost(message, optional, tag = "1")]
    pub black_card: ::std::option::Option<BlackCardInRound>,
    /// The white cards that were played for this round. Each entry includes the player who played the cards.
    #[prost(message, repeated, tag = "2")]
    pub white_played: ::std::vec::Vec<WhiteCardsPlayed>,
    /// The user who judged the round.
    #[prost(message, optional, tag = "3")]
    pub judge: ::std::option::Option<User>,
    /// The player who won the round.
    #[prost(message, optional, tag = "4")]
    pub winner: ::std::option::Option<Player>,
}
/// Game list view used when users are browsing games.
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GameInfo {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub game_id: std::string::String,
    /// Game configuration options.
    #[prost(message, optional, tag = "2")]
    pub config: ::std::option::Option<GameConfig>,
    /// The number of players currently in the game.
    /// This does not include artificial players.
    #[prost(int32, tag = "3")]
    pub player_count: i32,
    /// The current game owner.
    #[prost(message, optional, tag = "5")]
    pub owner: ::std::option::Option<User>,
    /// Whether the game is currently in progress.
    #[prost(bool, tag = "6")]
    pub is_running: bool,
    /// When the underlying game was created.
    #[prost(message, optional, tag = "7")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// When the game was last active.
    /// This only accounts for game-related actions, not users joining
    /// or leaving the game or sending messages in the chat.
    #[prost(message, optional, tag = "8")]
    pub last_activity_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message, serde::Serialize, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GameView {
    /// The unique id of the game.
    #[prost(string, tag = "1")]
    pub game_id: std::string::String,
    /// The config that was used to create this game.
    #[prost(message, optional, tag = "2")]
    pub config: ::std::option::Option<GameConfig>,
    /// What stage the game is in. This determines what actions are available.
    #[prost(enumeration = "game_view::Stage", tag = "3")]
    pub stage: i32,
    /// The player's current hand.
    #[prost(message, repeated, tag = "4")]
    pub hand: ::std::vec::Vec<PlayableWhiteCard>,
    /// All real and artificial players that are currently playing.
    /// Guaranteed to be ordered by join_time.
    #[prost(message, repeated, tag = "5")]
    pub players: ::std::vec::Vec<Player>,
    /// All real and artificial players that joined while the
    /// game was running (any Stage other than NOT_RUNNING).
    /// Guaranteed to be ordered by join_time.
    /// These players will be automatically moved from queued_players to
    /// players when StopGame is called (or if every user in the game
    /// calls VoteStartNextRound).
    #[prost(message, repeated, tag = "6")]
    pub queued_players: ::std::vec::Vec<Player>,
    /// All users that have been banned from the game.
    /// Always listed in the order they were banned.
    /// These users are unable to join the game.
    #[prost(message, repeated, tag = "7")]
    pub banned_users: ::std::vec::Vec<User>,
    /// The current judge for the round.
    /// This might be empty if the game stage is NOT_RUNNING
    /// (if there is a previous judge to show from the past game
    /// then it will be filled, otherwise it will be empty).
    /// If the game is in any other stage then this is always filled.
    /// When the game stage is PLAY_PHASE or JUDGE_PHASE
    /// it represents the current judge, and the judge does not change
    /// when the stage is changed from JUDGE_PHASE to ROUND_END_PHASE.
    /// Instead, it changes when the game stage changes from ROUND_END_PHASE to PLAY_PHASE.
    /// Whenever a game is started, a new random judge is selected.
    #[prost(message, optional, tag = "8")]
    pub judge: ::std::option::Option<User>,
    /// The current owner of the game.
    /// The game creator is the initial
    /// owner, but if the current owner
    /// leaves then ownership is transferred
    /// to the next user who has been
    /// in the game the longest.
    #[prost(message, optional, tag = "9")]
    pub owner: ::std::option::Option<User>,
    /// The white cards played this round.
    /// Data contained is dependent on the game stage.
    /// ----------------------------------------------
    /// When the game stage is PLAY_PHASE, each repeated value
    /// represents who has played this round. The 'user' field
    /// is filled for each value so that players know who has
    /// played, but the 'cards' field is empty so cards remain
    /// anonymous until after the judge phase. Values are
    /// in no particular order.
    ///
    /// When the game stage is JUDGE_PHASE, this will contain
    /// one value for each player that has played this round.
    /// For each repeated value, the 'cards' property will be
    /// filled but the 'user' property will be left empty and
    /// the order of values randomized so users don't know who
    /// submitted each card.
    ///
    /// When the game stage is ROUND_END_PHASE, this will
    /// contain one value for every player that played in
    /// the current round. For each repeated value, both
    /// 'user' and 'cards' properties will be filled.
    /// Values are in the same order as they were during
    /// the judge phase.
    ///
    /// When the game stage is NOT_RUNNING, behavior is the same as ROUND_END_PHASE
    /// except that the repeated field could contain no values. This happens if
    /// the game was just created and there is no past round to show cards for.
    /// Otherwise this will contain cards from the previous game
    /// that just ended (or was stopped).
    #[prost(message, repeated, tag = "10")]
    pub white_played: ::std::vec::Vec<WhiteCardsPlayed>,
    /// The black card that's active for the current round.
    /// This is empty if stage is NOT_RUNNING, otherwise
    /// this will contain the current card.
    #[prost(message, optional, tag = "11")]
    pub current_black_card: ::std::option::Option<BlackCardInRound>,
    /// Contains the winner of the most recent game.
    /// This is only filled when the stage is NOT_RUNNING
    /// and there is a winner to display. There is not
    /// always a winner to display, such as if the game
    /// was just created or if it was stopped
    /// before someone won.
    #[prost(message, optional, tag = "12")]
    pub winner: ::std::option::Option<Player>,
    /// Chat messages sent by players in the game.
    /// Contains, at most, the 100 most recent messages.
    #[prost(message, repeated, tag = "13")]
    pub chat_messages: ::std::vec::Vec<ChatMessage>,
    /// A list of previous rounds from the current game.
    /// This is cleared whenever the game is started/restarted.
    #[prost(message, repeated, tag = "14")]
    pub past_rounds: ::std::vec::Vec<PastRound>,
    /// When the game was created.
    #[prost(message, optional, tag = "15")]
    pub create_time: ::std::option::Option<super::google::protobuf::Timestamp>,
    /// When the game was last active.
    /// This only accounts for game-related actions, not users joining
    /// or leaving the game or sending messages in the chat.
    #[prost(message, optional, tag = "16")]
    pub last_activity_time: ::std::option::Option<super::google::protobuf::Timestamp>,
}
pub mod game_view {
    /// Represents what stage this game is currently in, which
    /// determines what actions are allowed for certain players.
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    #[derive(serde::Serialize, serde::Deserialize)]
    #[serde(rename_all = "snake_case")]
    pub enum Stage {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Game is not running.
        NotRunning = 1,
        /// Players (other than the judge) can play cards for the current round.
        PlayPhase = 2,
        /// The current judge can pick their favorite card(s).
        /// Cards from the play phase are visible but anonymous.
        JudgePhase = 3,
        /// Player scores are updated and players can view who played each card.
        RoundEndPhase = 4,
    }
}
#[doc = r" Generated client implementations."]
pub mod game_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct GameServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl GameServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> GameServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn search_games(
            &mut self,
            request: impl tonic::IntoRequest<super::SearchGamesRequest>,
        ) -> Result<tonic::Response<super::SearchGamesResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/SearchGames");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create_game(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/CreateGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn start_game(
            &mut self,
            request: impl tonic::IntoRequest<super::StartGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/StartGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn stop_game(
            &mut self,
            request: impl tonic::IntoRequest<super::StopGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/StopGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn join_game(
            &mut self,
            request: impl tonic::IntoRequest<super::JoinGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/JoinGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn leave_game(
            &mut self,
            request: impl tonic::IntoRequest<super::LeaveGameRequest>,
        ) -> Result<tonic::Response<super::super::google::protobuf::Empty>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/LeaveGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn kick_user(
            &mut self,
            request: impl tonic::IntoRequest<super::KickUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/KickUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn ban_user(
            &mut self,
            request: impl tonic::IntoRequest<super::BanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/BanUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn unban_user(
            &mut self,
            request: impl tonic::IntoRequest<super::UnbanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/UnbanUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn play_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::PlayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/PlayCards");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn unplay_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::UnplayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/UnplayCards");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn vote_card(
            &mut self,
            request: impl tonic::IntoRequest<super::VoteCardRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/VoteCard");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn vote_start_next_round(
            &mut self,
            request: impl tonic::IntoRequest<super::VoteStartNextRoundRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/VoteStartNextRound",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn add_artificial_player(
            &mut self,
            request: impl tonic::IntoRequest<super::AddArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/AddArtificialPlayer",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn remove_artificial_player(
            &mut self,
            request: impl tonic::IntoRequest<super::RemoveArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/RemoveArtificialPlayer",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create_chat_message(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateChatMessageRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/CreateChatMessage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_game_view(
            &mut self,
            request: impl tonic::IntoRequest<super::GetGameViewRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/GetGameView");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn list_white_card_texts(
            &mut self,
            request: impl tonic::IntoRequest<super::ListWhiteCardTextsRequest>,
        ) -> Result<tonic::Response<super::ListWhiteCardTextsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/ListWhiteCardTexts",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for GameServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for GameServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "GameServiceClient {{ ... }}")
        }
    }
}
