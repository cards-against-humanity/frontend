pub struct EnvironmentVariables {
    google_client_id: String,
    google_client_secret: String,
    google_redirect_uri: String,
    api_service_url: String,
    game_service_url: String,
    rabbitmq_uri: String,
    jwt_secret: String,
}

impl EnvironmentVariables {
    fn get_env_var_or_panic(key: &str) -> String {
        match std::env::var(key) {
            Ok(value) => value,
            _ => panic!("Missing environment variable `{}`.", key),
        }
    }

    pub fn get_google_client_id(&self) -> &str {
        &self.google_client_id
    }

    pub fn get_google_client_secret(&self) -> &str {
        &self.google_client_secret
    }

    pub fn get_google_redirect_uri(&self) -> &str {
        &self.google_redirect_uri
    }

    pub fn get_api_service_url(&self) -> &str {
        &self.api_service_url
    }

    pub fn get_game_service_url(&self) -> &str {
        &self.game_service_url
    }

    pub fn get_rabbitmq_uri(&self) -> &str {
        &self.rabbitmq_uri
    }

    pub fn get_jwt_secret(&self) -> &str {
        &self.jwt_secret
    }

    pub fn new() -> Self {
        Self {
            google_client_id: Self::get_env_var_or_panic("GOOGLE_CLIENT_ID"),
            google_client_secret: Self::get_env_var_or_panic("GOOGLE_CLIENT_SECRET"),
            google_redirect_uri: Self::get_env_var_or_panic("GOOGLE_REDIRECT_DOMAIN"),
            api_service_url: Self::get_env_var_or_panic("API_SERVICE_URL"),
            game_service_url: Self::get_env_var_or_panic("GAME_SERVICE_URL"),
            rabbitmq_uri: Self::get_env_var_or_panic("RABBITMQ_URI"),
            jwt_secret: Self::get_env_var_or_panic("JWT_SECRET"),
        }
    }
}
