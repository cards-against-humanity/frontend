use crate::environment::EnvironmentVariables;
use crate::jwt::JWTParser;
use crate::proto::crusty_cards_api::user_service_client::UserServiceClient;
use crate::proto::crusty_cards_api::{GetOrCreateUserRequest, OAuthCredentials, User};
use hyper::{http::Error, Body, Request, Response, Uri};
use oauth2::basic::BasicClient;
use oauth2::reqwest::http_client;
use oauth2::TokenResponse;
use oauth2::{
    AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, RedirectUrl, Scope, TokenUrl,
};
use percent_encoding::percent_decode_str;
use std::collections::HashMap;
use tonic::transport::Channel;

pub struct GoogleOAuth2Handler {
    client: BasicClient,
}

impl GoogleOAuth2Handler {
    pub fn new(env_vars: &EnvironmentVariables) -> Self {
        Self {
            client: BasicClient::new(
                ClientId::new(String::from(env_vars.get_google_client_id())),
                Some(ClientSecret::new(String::from(
                    env_vars.get_google_client_secret(),
                ))),
                AuthUrl::new("https://accounts.google.com/o/oauth2/v2/auth".to_string())
                    .expect("Invalid authorization endpoint URL"),
                Some(
                    TokenUrl::new("https://www.googleapis.com/oauth2/v3/token".to_string())
                        .expect("Invalid token endpoint URL"),
                ),
            )
            .set_redirect_url(
                RedirectUrl::new(String::from(env_vars.get_google_redirect_uri()))
                    .expect("Invalid redirect URL"),
            ),
        }
    }

    pub fn handle_google_oauth2_redirect_request(&self) -> Result<Response<Body>, Error> {
        let (authorize_url, _) = self
            .client
            .authorize_url(CsrfToken::new_random)
            .add_scope(Scope::new(
                "https://www.googleapis.com/auth/userinfo.profile".to_string(),
            ))
            .url();

        Response::builder()
            .status(302)
            .header("location", authorize_url.to_string())
            .body(Body::empty())
    }

    pub async fn handle_google_oauth2_callback_request(
        &self,
        req: Request<Body>,
        mut user_service: UserServiceClient<Channel>,
        jwt_parser: &JWTParser,
    ) -> Result<Response<Body>, Error> {
        let mut query_params = parse_query_params(&req.uri());
        let code = match query_params.remove("code") {
            Some(code) => AuthorizationCode::new(code),
            None => {
                return Response::builder()
                    .header("content-type", "text/html; charset=utf-8")
                    .body(Body::from(
                        "Unable to find/parse OAuth2 code. Try logging in again.",
                    ));
            }
        };

        // TODO - Perform this request asynchronously. Right now the `.request()` call blocks the thread.
        let token = match self.client.exchange_code(code).request(http_client) {
            Ok(token) => token,
            Err(_) => {
                return Response::builder()
                    .header("content-type", "text/html; charset=utf-8")
                    .body(Body::from(
                        "Encountered an error exchanging OAuth2 code. Try logging in again.",
                    ));
            }
        };

        let secret_bearer_token = format!("Bearer {}", token.access_token().secret());
        let user_info_res = match reqwest::Client::new()
            .get("https://www.googleapis.com/oauth2/v3/userinfo")
            .header("Authorization", secret_bearer_token)
            .send()
            .await
        {
            Ok(res) => res,
            Err(_) => {
                return Response::builder()
                    .header("content-type", "text/html; charset=utf-8")
                    .body(Body::from(
                        "Encountered an error fetching OAuth2 user profile. Try logging in again.",
                    ));
            }
        };

        let google_user_info: GoogleUserInfo = match user_info_res.json().await {
            Ok(google_user_info) => google_user_info,
            Err(_) => {
                return Response::builder()
                    .header("content-type", "text/html; charset=utf-8")
                    .body(Body::from(
                        "Encountered an error parsing OAuth2 user profile. Try logging in again.",
                    ));
            }
        };
        let get_or_create_user_request = GetOrCreateUserRequest {
            oauth_credentials: Some(OAuthCredentials {
                oauth_provider: String::from("google"),
                oauth_id: google_user_info.sub,
            }),
            user: Some(User {
                name: String::from(""),
                display_name: google_user_info.given_name,
                create_time: None,
                update_time: None,
            }),
        };

        let user = match user_service
            .get_or_create_user(tonic::Request::new(get_or_create_user_request))
            .await
        {
            Ok(res) => res.into_inner(),
            Err(_) => {
                return Response::builder()
                    .header("content-type", "text/html; charset=utf-8")
                    .body(Body::from(
                        "Encountered an error fetching user. Try logging in again.",
                    ));
            }
        };

        let jwt = jwt_parser.encode_jwt(&user.name);

        return Response::builder()
            .status(302)
            .header("location", "/gameList")
            .header(
                "set-cookie",
                format!("authToken={}; Path=/; Max-Age=604800", jwt),
            )
            .body(Body::empty());
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct GoogleUserInfo {
    pub sub: String,
    pub name: String,
    pub given_name: String,
    pub family_name: String,
    pub picture: String,
    pub locale: String,
}

fn parse_query_params(uri: &Uri) -> HashMap<String, String> {
    let query = match uri.query() {
        Some(query) => percent_decode_str(query).decode_utf8_lossy(),
        None => return HashMap::new(),
    };

    let query_params_list: Vec<&str> = query.split("&").collect();
    let mut query_params_map = HashMap::new();
    for param in query_params_list {
        let p: Vec<&str> = param.split("=").collect();
        // There should be one `=` per parameter.
        if p.len() == 2 {
            // The unwraps for `p.first()` and `p.last()` are safe since we already checked that `p.len() == 2`.
            query_params_map.insert(
                String::from(*p.first().unwrap()),
                String::from(*p.last().unwrap()),
            );
        }
    }
    query_params_map
}
