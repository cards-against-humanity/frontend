use hyper::{http::Error, Body, Response};
use std::collections::HashMap;
use std::include_bytes;

const HTML_BYTES: (&'static str, &'static [u8]) =
    ("index.html", include_bytes!("../content/static/index.html"));
const FAVICON_BYTES: (&'static str, &'static [u8]) = (
    "favicon.ico",
    include_bytes!("../content/static/favicon.ico"),
);

const CLIENT_WASM_BYTES: (&'static str, &'static [u8]) =
    ("client_bg.wasm", include_bytes!("../../pkg/client_bg.wasm"));
const CLIENT_JS_BYTES: (&'static str, &'static [u8]) =
    ("client.js", include_bytes!("../../pkg/client.js"));

const CORE_JS_BYTES: (&'static str, &'static [u8]) = (
    "core.js",
    include_bytes!("../../pkg/snippets/build/core.js"),
);
const MWC_BUTTON_BYTES: (&'static str, &'static [u8]) = (
    "mwc-button.js",
    include_bytes!("../../pkg/snippets/build/mwc-button.js"),
);
const MWC_CIRCULAR_PROGRESS_BYTES: (&'static str, &'static [u8]) = (
    "mwc-circular-progress.js",
    include_bytes!("../../pkg/snippets/build/mwc-circular-progress.js"),
);
const MWC_DIALOG_BYTES: (&'static str, &'static [u8]) = (
    "mwc-dialog.js",
    include_bytes!("../../pkg/snippets/build/mwc-dialog.js"),
);
const MWC_FAB_BYTES: (&'static str, &'static [u8]) = (
    "mwc-fab.js",
    include_bytes!("../../pkg/snippets/build/mwc-fab.js"),
);
const MWC_ICON_BUTTON_BYTES: (&'static str, &'static [u8]) = (
    "mwc-icon-button.js",
    include_bytes!("../../pkg/snippets/build/mwc-icon-button.js"),
);
const MWC_ICON_BYTES: (&'static str, &'static [u8]) = (
    "mwc-icon.js",
    include_bytes!("../../pkg/snippets/build/mwc-icon.js"),
);
const MWC_TOP_APP_BAR_FIXED_BYTES: (&'static str, &'static [u8]) = (
    "mwc-top-app-bar-fixed.js",
    include_bytes!("../../pkg/snippets/build/mwc-top-app-bar-fixed.js"),
);
const MWC_TOP_APP_BAR_BYTES: (&'static str, &'static [u8]) = (
    "mwc-top-app-bar.js",
    include_bytes!("../../pkg/snippets/build/mwc-top-app-bar.js"),
);

struct FileWithType<'a> {
    data: &'a [u8],
    mime_type: String,
}

impl<'a> FileWithType<'a> {
    pub fn new(raw_data: (&'a str, &'a [u8])) -> Self {
        let file_name = raw_data.0;
        let data = raw_data.1;
        let mime_type = Self::get_mime_type(file_name);
        Self { data, mime_type }
    }

    fn get_mime_type(file_name: &str) -> String {
        let s = match file_name.split(".").last().unwrap() {
            "js" => "application/javascript; charset=utf-8",
            "wasm" => "application/wasm",
            "html" => "text/html; charset=utf-8",
            "ico" => "image/x-icon",
            _ => panic!("File `{}` has an unhandled file extension.", file_name),
        };
        String::from(s)
    }

    pub fn create_response(&self) -> Result<Response<Body>, Error> {
        Response::builder()
            .header("content-type", &self.mime_type)
            .body(Body::from(self.data.to_vec()))
    }
}

pub struct StaticContentHandler {
    default_html: FileWithType<'static>,
    static_files: HashMap<&'static str, FileWithType<'static>>,
}

impl StaticContentHandler {
    pub fn new() -> Self {
        let mut static_files = HashMap::new();
        for raw_data in [
            HTML_BYTES,
            FAVICON_BYTES,
            CLIENT_WASM_BYTES,
            CLIENT_JS_BYTES,
            CORE_JS_BYTES,
            MWC_BUTTON_BYTES,
            MWC_CIRCULAR_PROGRESS_BYTES,
            MWC_DIALOG_BYTES,
            MWC_FAB_BYTES,
            MWC_ICON_BUTTON_BYTES,
            MWC_ICON_BYTES,
            MWC_TOP_APP_BAR_FIXED_BYTES,
            MWC_TOP_APP_BAR_BYTES,
        ]
        .iter()
        {
            static_files.insert(raw_data.0, FileWithType::new(*raw_data));
        }

        StaticContentHandler {
            default_html: FileWithType::new(HTML_BYTES),
            static_files,
        }
    }

    pub fn maybe_handle_request(&self, path: &str) -> Option<Result<Response<Body>, Error>> {
        Some(
            self.static_files
                .get(path.split("/").last().unwrap())?
                .create_response(),
        )
    }

    pub fn handle_request_with_default_html(&self) -> Result<Response<Body>, Error> {
        self.default_html.create_response()
    }
}
