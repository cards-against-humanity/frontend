mod proto {
    pub mod google {
        pub mod api {
            include!("proto/google.api.rs");
        }

        pub mod protobuf {
            include!("proto/google.protobuf.rs");
        }
    }

    pub mod crusty_cards_api {
        include!("proto/crusty_cards_api.rs");
    }
}

mod environment;
mod google_oauth2_handler;
mod jwt;
mod static_content_handler;

use crate::jwt::JWTParser;
use cards_frontend_shared::rpc::Status;
use environment::EnvironmentVariables;
use google_oauth2_handler::GoogleOAuth2Handler;
use hyper::service::{make_service_fn, service_fn};
use hyper::{http::Error, Body, Request, Response, Server};
use proto::crusty_cards_api::cardpack_service_client::CardpackServiceClient;
use proto::crusty_cards_api::game_service_client::GameServiceClient;
use proto::crusty_cards_api::user_service_client::UserServiceClient;
use proto::crusty_cards_api::*;
use serde::Serialize;
use static_content_handler::StaticContentHandler;
use std::collections::HashMap;
use std::future::Future;
use std::net::SocketAddr;
use std::sync::Arc;
use tonic::transport::Channel;

struct HandlerState {
    user_service: UserServiceClient<Channel>,
    cardpack_service: CardpackServiceClient<Channel>,
    game_service: GameServiceClient<Channel>,
    static_content_handler: StaticContentHandler,
    google_oauth2_handler: GoogleOAuth2Handler,
    jwt_parser: JWTParser,
}

impl HandlerState {
    async fn new() -> Self {
        let env_vars = EnvironmentVariables::new();

        let user_service = UserServiceClient::connect(String::from(env_vars.get_api_service_url()))
            .await
            .unwrap();
        let cardpack_service =
            CardpackServiceClient::connect(String::from(env_vars.get_api_service_url()))
                .await
                .unwrap();
        let game_service =
            GameServiceClient::connect(String::from(env_vars.get_game_service_url()))
                .await
                .unwrap();

        let static_content_handler = StaticContentHandler::new();

        let google_oauth2_handler = GoogleOAuth2Handler::new(&env_vars);

        let jwt_parser = JWTParser::new(env_vars.get_jwt_secret());

        Self {
            user_service,
            cardpack_service,
            game_service,
            static_content_handler,
            google_oauth2_handler,
            jwt_parser,
        }
    }
}

#[tokio::main]
async fn main() {
    let port = 3000;
    let addr = SocketAddr::from(([127, 0, 0, 1], port));

    let handler_state = Arc::from(HandlerState::new().await);

    let make_svc = make_service_fn(move |_| {
        let handler_state = handler_state.clone();

        async {
            Ok::<_, Error>(service_fn(move |req| {
                handle_request(req, handler_state.clone())
            }))
        }
    });

    let server = Server::bind(&addr).serve(make_svc);
    println!("Server started on port {}.", port);

    if let Err(e) = server.await {
        eprintln!("Server error: {}", e);
    }
}

async fn handle_request(
    req: Request<Body>,
    handler_state: Arc<HandlerState>,
) -> Result<Response<Body>, Error> {
    let user_name_or = get_user_name_from_request(&req, &handler_state.jwt_parser);

    if req.uri().path() == "/auth/google" {
        return handler_state
            .google_oauth2_handler
            .handle_google_oauth2_redirect_request();
    }

    if req.uri().path() == "/auth/google/callback" {
        return handler_state
            .google_oauth2_handler
            .handle_google_oauth2_callback_request(
                req,
                handler_state.user_service.clone(),
                &handler_state.jwt_parser,
            )
            .await;
    }

    if req.uri().path() == "/logout" {
        return Response::builder()
            .status(302)
            .header("location", "/login")
            .header(
                "set-cookie",
                "authToken=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT",
            )
            .body(Body::empty());
    }

    if req.uri().path().starts_with("/api") {
        let (parts, body) = req.into_parts();
        return handle_api_request(
            parts.uri.path().split_at(4).1,
            body,
            handler_state.user_service.clone(),
            handler_state.cardpack_service.clone(),
            handler_state.game_service.clone(),
            user_name_or,
        )
        .await;
    }

    match handler_state
        .static_content_handler
        .maybe_handle_request(req.uri().path())
    {
        Some(res) => return res,
        None => {}
    };

    handler_state
        .static_content_handler
        .handle_request_with_default_html()
}

fn get_user_name_from_request(req: &Request<Body>, jwt_parser: &JWTParser) -> Option<String> {
    let cookie_string: &str = match req.headers().get("cookie")?.to_str() {
        Ok(cookie_string) => cookie_string,
        Err(_) => return None,
    };
    let cookie_list: Vec<&str> = cookie_string.split(";").map(|c| c.trim()).collect();
    let cookie_map: HashMap<String, String> = cookie_list
        .into_iter()
        .filter_map(|c| {
            let split: Vec<&str> = c.split("=").map(|v| v.trim()).collect();
            if split.len() == 2 {
                Some((
                    String::from(*split.first().unwrap()),
                    String::from(*split.last().unwrap()),
                ))
            } else {
                None
            }
        })
        .collect();
    let auth_token = cookie_map.get("authToken")?;
    let user_name = jwt_parser.decode_jwt(auth_token);
    Some(user_name)
}

async fn handle_exact_api_request<
    Req: prost::Message + Default,
    Res: prost::Message + Serialize,
    Fut,
>(
    rpc: impl FnOnce(Req) -> Fut,
    body: Vec<u8>,
) -> Result<Response<Body>, Error>
where
    Fut: Future<Output = Result<tonic::Response<Res>, tonic::Status>>,
{
    let incoming_request =
        cards_frontend_shared::rpc::binary_decode_prost_message::<Req>(body).unwrap();
    encode_proto_response(rpc(incoming_request).await)
}

fn encode_proto_response<Res: prost::Message + Serialize>(
    tonic_response_or: Result<tonic::Response<Res>, tonic::Status>,
) -> Result<Response<Body>, Error> {
    let response_or: Result<Res, Status> = match tonic_response_or {
        Ok(response) => Ok(response.into_inner()),
        Err(err) => Err(err.into()),
    };
    Response::builder()
        .header("content-type", "text/plain;charset=UTF-8")
        .status(200)
        .body(Body::from(
            cards_frontend_shared::rpc::binary_encode_prost_message_or_status(response_or),
        ))
}

async fn handle_api_request(
    path: &str,
    body: Body,
    mut user_service: UserServiceClient<Channel>,
    mut cardpack_service: CardpackServiceClient<Channel>,
    mut game_service: GameServiceClient<Channel>,
    user_name_or: Option<String>,
) -> Result<Response<Body>, Error> {
    let body_bytes = hyper::body::to_bytes(body).await.unwrap();

    // TODO - Add API route security based on the user who is making the request for each route in the match statement below.
    match path {
        "/user/me" => {
            let res_or = match user_name_or {
                Some(name) => user_service.get_user(GetUserRequest { name }).await,
                None => Err(tonic::Status::not_found("User is not authenticated.")),
            };
            encode_proto_response(res_or)
        }
        "/userSettings/me" => {
            let res_or = match user_name_or {
                Some(name) => {
                    user_service
                        .get_user_settings(GetUserSettingsRequest {
                            name: format!("{}/settings", name),
                        })
                        .await
                }
                None => Err(tonic::Status::not_found("User is not authenticated.")),
            };
            encode_proto_response(res_or)
        }
        // User service RPCs.
        "/userService/getUser" => {
            handle_exact_api_request(
                |req: GetUserRequest| user_service.get_user(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/updateUser" => {
            handle_exact_api_request(
                |req: UpdateUserRequest| user_service.update_user(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/getUserSettings" => {
            handle_exact_api_request(
                |req: GetUserSettingsRequest| user_service.get_user_settings(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/updateUserSettings" => {
            handle_exact_api_request(
                |req: UpdateUserSettingsRequest| user_service.update_user_settings(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/getUserProfileImage" => {
            handle_exact_api_request(
                |req: GetUserProfileImageRequest| user_service.get_user_profile_image(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/updateUserProfileImage" => {
            handle_exact_api_request(
                |req: UpdateUserProfileImageRequest| user_service.update_user_profile_image(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/userSearch" => {
            handle_exact_api_request(
                |req: UserSearchRequest| user_service.user_search(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/userService/autocompleteUserSearch" => {
            handle_exact_api_request(
                |req: AutocompleteUserSearchRequest| user_service.autocomplete_user_search(req),
                body_bytes.to_vec(),
            )
            .await
        }
        // Cardpack service RPCs.
        "/cardpackService/createCustomCardpack" => {
            handle_exact_api_request(
                |req: CreateCustomCardpackRequest| cardpack_service.create_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/getCustomCardpack" => {
            handle_exact_api_request(
                |req: GetCustomCardpackRequest| cardpack_service.get_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listCustomCardpacks" => {
            handle_exact_api_request(
                |req: ListCustomCardpacksRequest| cardpack_service.list_custom_cardpacks(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/updateCustomCardpack" => {
            handle_exact_api_request(
                |req: UpdateCustomCardpackRequest| cardpack_service.update_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/deleteCustomCardpack" => {
            handle_exact_api_request(
                |req: DeleteCustomCardpackRequest| cardpack_service.delete_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/createCustomBlackCard" => {
            handle_exact_api_request(
                |req: CreateCustomBlackCardRequest| cardpack_service.create_custom_black_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/createCustomWhiteCard" => {
            handle_exact_api_request(
                |req: CreateCustomWhiteCardRequest| cardpack_service.create_custom_white_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listCustomBlackCards" => {
            handle_exact_api_request(
                |req: ListCustomBlackCardsRequest| cardpack_service.list_custom_black_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listCustomWhiteCards" => {
            handle_exact_api_request(
                |req: ListCustomWhiteCardsRequest| cardpack_service.list_custom_white_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/updateCustomBlackCard" => {
            handle_exact_api_request(
                |req: UpdateCustomBlackCardRequest| cardpack_service.update_custom_black_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/updateCustomWhiteCard" => {
            handle_exact_api_request(
                |req: UpdateCustomWhiteCardRequest| cardpack_service.update_custom_white_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/deleteCustomBlackCard" => {
            handle_exact_api_request(
                |req: DeleteCustomBlackCardRequest| cardpack_service.delete_custom_black_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/deleteCustomWhiteCard" => {
            handle_exact_api_request(
                |req: DeleteCustomWhiteCardRequest| cardpack_service.delete_custom_white_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/batchCreateCustomBlackCards" => {
            handle_exact_api_request(
                |req: BatchCreateCustomBlackCardsRequest| {
                    cardpack_service.batch_create_custom_black_cards(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/batchCreateCustomWhiteCards" => {
            handle_exact_api_request(
                |req: BatchCreateCustomWhiteCardsRequest| {
                    cardpack_service.batch_create_custom_white_cards(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/getDefaultCardpack" => {
            handle_exact_api_request(
                |req: GetDefaultCardpackRequest| cardpack_service.get_default_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listDefaultCardpacks" => {
            handle_exact_api_request(
                |req: ListDefaultCardpacksRequest| cardpack_service.list_default_cardpacks(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listDefaultBlackCards" => {
            handle_exact_api_request(
                |req: ListDefaultBlackCardsRequest| cardpack_service.list_default_black_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listDefaultWhiteCards" => {
            handle_exact_api_request(
                |req: ListDefaultWhiteCardsRequest| cardpack_service.list_default_white_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/undeleteCustomCardpack" => {
            handle_exact_api_request(
                |req: UndeleteCustomCardpackRequest| cardpack_service.undelete_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/undeleteCustomBlackCard" => {
            handle_exact_api_request(
                |req: UndeleteCustomBlackCardRequest| {
                    cardpack_service.undelete_custom_black_card(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/undeleteCustomWhiteCard" => {
            handle_exact_api_request(
                |req: UndeleteCustomWhiteCardRequest| {
                    cardpack_service.undelete_custom_white_card(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/listFavoritedCustomCardpacks" => {
            handle_exact_api_request(
                |req: ListFavoritedCustomCardpacksRequest| {
                    cardpack_service.list_favorited_custom_cardpacks(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/likeCustomCardpack" => {
            handle_exact_api_request(
                |req: LikeCustomCardpackRequest| cardpack_service.like_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/unlikeCustomCardpack" => {
            handle_exact_api_request(
                |req: UnlikeCustomCardpackRequest| cardpack_service.unlike_custom_cardpack(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/cardpackService/checkDoesUserLikeCustomCardpack" => {
            handle_exact_api_request(
                |req: CheckDoesUserLikeCustomCardpackRequest| {
                    cardpack_service.check_does_user_like_custom_cardpack(req)
                },
                body_bytes.to_vec(),
            )
            .await
        }
        // Game service RPCs.
        "/gameService/searchGames" => {
            handle_exact_api_request(
                |req: SearchGamesRequest| game_service.search_games(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/createGame" => {
            handle_exact_api_request(
                |req: CreateGameRequest| game_service.create_game(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/startGame" => {
            handle_exact_api_request(
                |req: StartGameRequest| game_service.start_game(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/stopGame" => {
            handle_exact_api_request(
                |req: StopGameRequest| game_service.stop_game(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/joinGame" => {
            handle_exact_api_request(
                |req: JoinGameRequest| game_service.join_game(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/leaveGame" => {
            handle_exact_api_request(
                |req: LeaveGameRequest| game_service.leave_game(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/kickUser" => {
            handle_exact_api_request(
                |req: KickUserRequest| game_service.kick_user(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/banUser" => {
            handle_exact_api_request(
                |req: BanUserRequest| game_service.ban_user(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/unbanUser" => {
            handle_exact_api_request(
                |req: UnbanUserRequest| game_service.unban_user(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/playCards" => {
            handle_exact_api_request(
                |req: PlayCardsRequest| game_service.play_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/unplayCards" => {
            handle_exact_api_request(
                |req: UnplayCardsRequest| game_service.unplay_cards(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/voteCard" => {
            handle_exact_api_request(
                |req: VoteCardRequest| game_service.vote_card(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/voteStartNextRound" => {
            handle_exact_api_request(
                |req: VoteStartNextRoundRequest| game_service.vote_start_next_round(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/addArtificialPlayer" => {
            handle_exact_api_request(
                |req: AddArtificialPlayerRequest| game_service.add_artificial_player(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/removeArtificialPlayer" => {
            handle_exact_api_request(
                |req: RemoveArtificialPlayerRequest| game_service.remove_artificial_player(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/createChatMessage" => {
            handle_exact_api_request(
                |req: CreateChatMessageRequest| game_service.create_chat_message(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/getGameView" => {
            handle_exact_api_request(
                |req: GetGameViewRequest| game_service.get_game_view(req),
                body_bytes.to_vec(),
            )
            .await
        }
        "/gameService/listWhiteCardTexts" => {
            handle_exact_api_request(
                |req: ListWhiteCardTextsRequest| game_service.list_white_card_texts(req),
                body_bytes.to_vec(),
            )
            .await
        }
        _ => get_404_response("Api path not found!".to_string()),
    }
}

fn get_404_response(message: String) -> Result<Response<Body>, Error> {
    Response::builder()
        .header("content-type", "text/html; charset=utf-8")
        .status(404)
        .body(Body::from(message))
}
